//Fibonacci Series using Recursion
#include<stdio.h>

int cache[40];

int fib(int n) {
  if (n <= 1)
    return n;
  if (cache[n-1] == 0) {
    cache[n-1] = fib(n-1) + fib(n-2);
  }
  return cache[n-1];
}

int main () {
  int i;

  for (i = 0; i < 40; i++) {
    cache[i] = 0;
  }

  printf("%d\n", fib(40));
  return 0;
}
