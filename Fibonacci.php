<?php
class Fibonacci
{
  public static $cache = [];
  
  public static function fib($n) {
    if ($n <= 1)
      return $n;
    if (empty(self::$cache[$n-1])) {
      self::$cache[$n-1] = self::fib($n-1) + self::fib($n-2);
    }
    return self::$cache[$n-1];
  }
}

echo Fibonacci::fib(40) . "\n";

