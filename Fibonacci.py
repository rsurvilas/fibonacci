cache = []
for i in range(40):
  cache.append(0)

def fib(n):
  if n<=1:
    return n
  if cache[n-1] == 0:
    cache[n-1] = fib(n-1) + fib(n-2)
  return cache[n-1]

print(fib(40))
