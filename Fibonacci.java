public class Fibonacci {
  private static int[] cache;

  public static void main(String[] args) {
      cache = new int[40];
      System.out.printf("%d\n", fib(40));
  }

  private static int fib(int n) {
    if (n <= 1)
      return n;
    if (cache[n-1] == 0) {
      cache[n-1] = fib(n-1) + fib(n-2);
    }
    return cache[n-1];
  }
}

