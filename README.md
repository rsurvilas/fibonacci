# README #

Before starting this lame benchmark, You have to have C, Java, ant Python on Your machine. Linux is preferable for convenient usage.

### How to start? ###
* Compile all source code (except interpreted lang)
* Run Benchmark.py

### Results ###
* This benchmark runs the same code for a 100 times. Average execution time is shown at the end of the benchmark.

### Want to contribute? ###
Feel free to contribute with some other interesting tasks or methods for benchmarking.