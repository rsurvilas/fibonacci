#!/usr/bin/env python3.4

import time
import os

print("Welcome to lame benchmark test :)")

commands = ["java Fibonacci", "./Fibonacci.o", "python3.4 Fibonacci.py", "php Fibonacci.php"]

def executeCommand(cmd):
  start_time = time.time();
  os.system(cmd)
  return time.time() - start_time

runs = 100
for cmd in commands:
  avg = 0
  for i in range(runs):
    avg += executeCommand(cmd)
  print(cmd + " average time: " + str(avg/runs))

